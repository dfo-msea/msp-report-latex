\documentclass{article}
\usepackage{geometry}
\geometry{letterpaper, portrait, margin=1.3in}
\usepackage{fancyhdr}
\usepackage{graphicx}
% Parskip prevents paragraph indentation.
\usepackage{parskip}
\usepackage{tocloft}
% Bibliography package and file.
\usepackage[authoryear]{natbib}
% Modify section style to be centered.
\usepackage{titlesec}
% This is used for figure placement.
\usepackage{float}
\titleformat{\section}[block]{\sffamily\Large\bfseries\filcenter}{\thesection}{1em}{}
% Do not number sections, but include in TOC.
\setcounter{secnumdepth}{0}
% Hyperlink the table of contents entries. Remove boxes around links.
\usepackage[hidelinks]{hyperref}
\usepackage{xurl}
\hypersetup{
    linktoc=all,
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue,
    breaklinks=true,
    bookmarksnumbered=true
}
  % --- VARIABLES/COMMANDS ---
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\contentsname}{TABLE OF CONTENTS}
\newcommand{\docTitle}{OPEN DATA RECORDS PUBLISHED TO SUPPORT\\MARINE SPATIAL PLANNING IN PACIFIC REGION}
% Define authors with and without superscripts
\newcommand{\docAuthorsTitle}{%
  \mbox{Carrie Robb}, \mbox{Selina Agbayani}, \mbox{Chelsea Greenberg}, \mbox{Cole Fields}, \mbox{Lu Guan}, \newline \mbox{Samantha Huntington}, \mbox{Steven Schut}, \mbox{Beatrice Proudfoot}, and \mbox{Cathryn Murray}\\%
}
\newcommand{\docAuthorsDescription}{Carrie Robb\textsuperscript{1}, Selina Agbayani\textsuperscript{2}, Chelsea Greenberg\textsuperscript{2}, Cole Fields\textsuperscript{2}, Lu Guan\textsuperscript{2}, Samantha Huntington\textsuperscript{2}, Steven Schut\textsuperscript{3}, Beatrice Proudfoot\textsuperscript{3}, and Cathryn Murray\textsuperscript{2}\\}
\newcommand{\docAddress}{Fisheries and Oceans Canada\\
                                                    Science Branch, Pacific Region\\
                                                    Pacific Biological Station\\
                                                    3190 Hammond Bay Road\\
                                                    Nanaimo, British Columbia\\
                                                    V9T 6N7\\}
\newcommand{\docNumber}{3656}
\newcommand{\docYear}{2025}
\newcommand{\docTechDesc}{Canadian Technical Report of\\Fisheries and Aquatic Sciences \docNumber\\}
\newcommand{\docCitation}{Robb, C., Agbayani, S., Greenberg, C., Fields, C., Guan, L., Huntington, S., Schut, S., \mbox{Proudfoot}, B., and Murray, C. \docYear. Open Data Records Published to Support Marine Spatial Planning in Pacific Region. Can. Tech. Rep. Fish. Aquat. Sci. \docNumber: xi + 167 p.\\}
\newcommand{\docCopyright}{© His Majesty the King in Right of Canada, as represented by the Minister of the Department of Fisheries and Oceans, \docYear.\\Cat. No. Fs97-6/3656E-PDF    ISBN 978-0-660-74966-2    ISSN 1488-5379\\}
\usepackage[sfdefault,medium]{inter}
\renewcommand\familydefault{\sfdefault}

\begin{document}
  % --- TITLE PAGE ---
  \pagenumbering{gobble}  % No page number on title page
  \input{tex_files/title_en}
  \pagebreak

  % --- TECH REPORT DESCRIPTION PAGE ---
  \input{tex_files/tech_description}
  \pagebreak

  % --- START ROMAN NUMERAL NUMBERING ---
  \pagenumbering{roman}

  % --- DOCUMENT DESCRIPTION PAGE  ---
  \input{tex_files/doc_description}
  \pagebreak

  % --- COPYRIGHT PAGE  ---
  \input{tex_files/copyright}
  \pagebreak

  % --- TABLE OF CONTENTS ---
  \tableofcontents
  \pagebreak

  % --- LIST OF FIGURES ---
  \phantomsection
  \addcontentsline{toc}{section}{LIST OF FIGURES}
  \listoffigures
  \pagebreak

  % --- ABSTRACT ---
  \input{tex_files/abstract}
  \pagebreak

  % --- INTRODUCTION ---
  \pagenumbering{arabic} 
  \input{tex_files/introduction_en}
  \pagebreak

\BLOCK{ macro escape_latex(text) }
  \VAR{ text.replace('_', '\\_') }
\BLOCK{ endmacro }

\section{SPATIAL DATASETS}
  % --- METADATA ENTRIES ---
  % Iterate datasets retrieved from Open Maps, filling in the template.
  \BLOCK{ for key, value in dataset_names.items() }
    \subsection{\VAR{key}}
    \BLOCK{ for n in value }
      \subsubsection{\VAR{ escape_latex(n) }}
        \VAR{notes.get(key)[loop.index0]}\\\\
        \textbf{Record ID:} \VAR{open_ids.get(key)[loop.index0]}\\\\
        \textbf{URL:}\\\\
        \url{\VAR{url_dict_en.get(key)[loop.index0]}}\\\\
        \textbf{Contact Email:} \VAR{maintainer.get(key)[loop.index0]}\\\\
        \textbf{Date Published:} \VAR{release_date.get(key)[loop.index0]}\\\\
        \textbf{Topic Category:} \VAR{topic_category.get(key)[loop.index0]}\\\\
        \textbf{Keywords:} \VAR{keywords.get(key)[loop.index0]}\\\\
        % Iterate over map_plot and include plots only when category is equal to key and title is equal to n
        \BLOCK{ for category, category_data in map_plot.items() }
            \BLOCK{ if category == key }
                \BLOCK{ for title, paths_and_captions in category_data.items() }
                    \BLOCK{ if title == n }
                      \textbf{Example map(s) of available datasets:}\\\\
                        \BLOCK{ for path, caption, short_caption in paths_and_captions }
                          \begin{figure}[H]
                            \centering
                            \includegraphics[scale=0.75]{\VAR{path}}
                            \caption[\VAR{short_caption}]{\VAR{caption}}
                          \end{figure}
                          \pagebreak
                       \BLOCK{ endfor }
                    \BLOCK{ endif }
                \BLOCK{ endfor }
            \BLOCK{ endif }
        \BLOCK{ endfor }
    \BLOCK{ endfor }
  \BLOCK{ endfor }
  \pagebreak

  % --- CONCLUSION ---
  \input{tex_files/conclusion_en}
  \pagebreak

  % --- ACKNOWLEDGEMENTS ---
  \input{tex_files/acknowledgements_en}
  \pagebreak

  % --- BIBLIOGRAPHY ---
  \phantomsection
  \addcontentsline{toc}{section}{REFERENCES}
  \renewcommand{\refname}{REFERENCES}
  \bibliographystyle{cjfas}
  \bibliography{references_en}

\end{document}
