""" The purpose of this script is to generate a PDF report.
     It reads data from a CSV file with IDs that correspond to Open Maps records.
     For each record, it gets the metadata and creates maps of example layer(s) for the spatial
     data that are included in the report.
     
     VIRTUAL ENV: ~~msp-venv~~ NO! Must use gdal2 env which has gdal version 3.7.3.
     This is mandatory for File Geodatabase Raster layers.
"""
from urllib.error import HTTPError
import os
import csv
import subprocess
import urllib.request as request
import json
from datetime import datetime
import traceback
import zipfile
from osgeo import gdal
from osgeo import osr
import geopandas as gp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import netCDF4 as nc
import cartopy.crs as ccrs
import contextily as ctx
import fiona
import rasterio as rio
from pyproj.crs import CRS
from rasterio.plot import plotting_extent
import numpy as np
import glob
import shutil
import logging
import pathlib
import re
import settings
import ssl
import re
import requests
import time
import py7zr


def logger_setup(output_directory):
    if not os.path.exists(output_directory):
        os.mkdir(output_directory)
    logging.basicConfig(
        filename=os.path.join(
            output_directory, f'msp-report-{datetime.now():%Y-%m-%d}.log'),
        format='| %(levelname)-7s| %(asctime)s | %(message)s',
        level='INFO',  # numeric value: 20
    )
    logger = logging.getLogger('msp-report')
    return logger


# Create logger object.
logger = logger_setup(settings.LOG_DIR)


def get_french_month(month_number):
    """Convert month number to French month name."""
    french_months = {
        1: "janvier",
        2: "février",
        3: "mars",
        4: "avril", 
        5: "mai",
        6: "juin",
        7: "juillet",
        8: "août",
        9: "septembre",
        10: "octobre",
        11: "novembre",
        12: "décembre"
    }
    return french_months[month_number]


def wrap_http_urls(text):
    """ Given a string, check for doi and wrap in \\url{} such as \\url{https://doi.org/10.5067/AQUA/MODIS/L2/OC/2018} """
    if not text or ('http://' not in text and 'https://' not in text):
        return text
    # Combined pattern for both http and https
    # This pattern will match URLs that:
    # - Start with http:// or https://
    # - Continue with any non-whitespace characters
    # - Include hyphens
    # - Don't break at hyphens or periods
    search_text = r'((?:http|https)://[^\s]+(?:-[^\s]+)*)'
    # Replace URLs with \url{} wrapper
    wrapped_text = re.sub(search_text, r'\\url{\1}', text)
    return wrapped_text


def get_title(png_string):
    """ Make human-friendly title from PNG_STRING.
    https://www.geeksforgeeks.org/python-split-camelcase-string-to-individual-strings/
        >>> get_title('CamelCase_string')
        'Camel Case String'
        >>> get_title('HabitatRichness_hotspot_nearshore')
        'Habitat Richness Hotspot Nearshore'
        >>> get_title('Variable42Name_XYZ')
        'Variable 42 Name XYZ'
        >>> get_title('SST_mean_month_01_1990_2020')
        'SST Mean Month 01 1990 2020'
    """
    words = re.findall(r'[A-Za-z]+|\d+', png_string)
    camel_string = '  '.join(
        [word.title() if word[0].islower() or word.isdigit() else word for word in words])
    return ' '.join(re.findall(r'[A-Z]?[a-z]+|[A-Z]+\d*|\d+', camel_string))


def get_title_upper(png_string):
    """ Make uppercase title from PNG_STRING.
    https://www.geeksforgeeks.org/python-split-camelcase-string-to-individual-strings/
        >>> get_title_upper('CamelCase_string')
        'CAMEL CASE STRING'
        >>> get_title_upper('HabitatRichness_hotspot_nearshore')
        'HABITAT RICHNESS HOTSPOT NEARSHORE'
    """
    png_string = get_title(png_string).upper()
    if 'CAN OE' in png_string:
        png_string = png_string.replace('CAN OE', 'CANOE')
    return png_string


def load_base_lyr(base_lyr_path):
    """ Load a vector shape from filepath as geodataframe. """
    try:
        # Load base layer.
        logger.info('Loading base layer...')
        base = gp.read_file(base_lyr_path)
        logger.info('Base layer loaded successfully...')
        return base
    except Exception:
        logger.error('Error loading base data ({}).'.format(base_lyr_path))
        logger.error(traceback.format_exc())


def format_sections(text):
    """
    1. Add colons to section headers if missing
    2. Italicize specific words
    Args:
        text (str): The text content from open data
    Returns:
        str: Formatted text
    """
    # Define section headers that should end with colon
    sections = ['Description', 'Methods', 'Uncertainties', 'Data Sources', 'References']
    # 1. Add colons to section headers if missing
    # Look for section headers at the start of a line followed by a newline
    for section in sections:
        # Pattern matches: section name at start of line or after newline,
        # not followed by a colon, followed by newline
        pattern = f'(^|\\n)({section})(?!:)(\\n)'
        text = re.sub(pattern, f'\\1\\2:\\3', text)
    # 2. Add LaTeX italics formatting
    for word in sections:
        text = text.replace(word, f'\\textit{{{word}}}')
    return text


def get_om_results(open_map_id):
    """ Get JSON metadata results using CKAN API from open.canada.ca. """
    try:
        logger.info(
            f'----------Looking for metadata on Open Maps with record with ID: {open_map_id}----------')
        # Create a custom context that handles SSL verification
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        response = request.urlopen(settings.PACKAGE_SHOW + open_map_id, context=context)
        html = response.read()
        result = json.loads(html.decode(settings.ENCODING)).get('result')
        return clean_results(result)
    except HTTPError as e:
        logger.error(
            f'HTTP error retrieving metadata from Open Maps for record with ID {open_map_id}')
        logger.error(str(e))
    except Exception as e:
        logger.error(f'Error retrieving metadata for ID {open_map_id}: {str(e)}')


def has_spatial_resources(data_dict):
    """ Return whether DATA_DICT has spatial resourcs. """
    resources = data_dict.get('resources')
    for r in resources:
        if r.get('format').lower() in settings.SPATIAL_TYPES:
            return True
    logger.warning(f'{data_dict.get("title")} has no spatial resources associated with it. REMOVING.')
    return False


def replace_all(text, dict):
    """
    For each item in dict, search the key in the text string and replace with the corresponding dict value.
    :param text: string of text
    :param dict: dictionary with key (string to be replaced) and value (string to replace current string with)
    :return: modified string with replaced text
    https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """
    for i, j in dict.items():
        text = text.replace(i, j)
    return text


def get_om_ids(id_file):
    """ Return list of unique IDs from ID_FILE. """
    try:
        logger.info(f'Getting list of IDs from {id_file}')
        with open(id_file, 'r', encoding=settings.ENCODING) as f:
            content = f.readlines()
        # Remove whitespace characters like `\n` at the end of each line. Remove blank lines. Return set.
        content = [x.strip() for x in content]
        content = list(filter(None, content))
        unique_ids = set(content)
        logger.info(f'{len(unique_ids)} IDs retrieved from {id_file}')
        return unique_ids
    except:
        logger.error(
            'Error loading IDs from text file. Each ID must be on its own line.')
        logger.error(traceback.format_exc())


def create_id_dict(id_file):
    """ Create nested dictionary of keys (IDs) and values (dict of EX_RES and EX_LYRS). """
    try:
        logger.info(f'Creating nested dictionary of IDs from {id_file}')
        data_dict = {}
        # Open the CSV file and read its contents
        with open(id_file, newline='', mode='r', encoding=settings.ENCODING) as csvfile:
            reader = csv.DictReader(csvfile)
            # Iterate over each row in the CSV file
            for row in reader:
                id_value = row['id']
                ds_title = row['title']
                ex_res_value = row['ex_res']
                ex_lyrs_value = row['ex_lyrs']
                ex_lyrs_caption = row['ex_captions']
                fgdb_raster = row['fgdb_raster']
                fgdb_both = row['fgdb_both']
                flip_rasters = row['flip_rasters']
                topic = row['topic']
                include_map = row['include_map']
                if ex_lyrs_value == '[]':
                    ex_lyrs_value = []
                elif id_value == '661f58cc-e1c3-4249-ba62-08de5b464fe1':
                    ex_lyrs_value = json.loads(ex_lyrs_value)
                else:
                    ex_lyrs_value = ex_lyrs_value.split(',')
                    ex_lyrs_value = [lyr.strip() for lyr in ex_lyrs_value]
                ex_lyrs_caption = ex_lyrs_caption.split('!!!')
                ex_lyrs_caption = [lyr.strip() for lyr in ex_lyrs_caption]    
                # Create a new dictionary for the 'id'
                data_dict[id_value] = {ex_res_value: ex_lyrs_value,
                                        'title': ds_title,
                                        'captions': ex_lyrs_caption, 
                                        'fgdb_raster': int(fgdb_raster), 
                                        'fgdb_both': int(fgdb_both), 
                                        'flip_rasters': int(flip_rasters),
                                        'topic': topic,
                                        'include_map': int(include_map)}
        return data_dict
    except:
        logger.error('Error processing data from file.')
        logger.error(traceback.format_exc())


def clean_results(metadata_results):
    """Clean the strings to remove or replace characters that are problematic in LaTeX."""
    fields_to_clean = ['notes_translated', 'title_translated', 'topic_category']
    # Create a modified CHAR_MAP for titles that doesn't escape underscores
    title_char_map = settings.CHAR_MAP.copy()
    title_char_map.pop('_')  # Remove underscore mapping for titles
    for field in fields_to_clean:
        if field == 'topic_category':
            items = metadata_results.get(field)
            cleaned_items = [replace_all(i, settings.CHAR_MAP).strip() for i in items]
            metadata_results[field] = cleaned_items
        else:
            text = metadata_results.get(field).get('en')
            # For titles, use the modified map that doesn't escape underscores
            if field == 'title_translated':
                text = replace_all(text, title_char_map).strip()
            else:
                text = replace_all(text, settings.CHAR_MAP).strip()
            text = format_sections(text)
            metadata_results[field]['en'] = text
            # Same for French
            text = metadata_results.get(field).get('fr')
            if field == 'title_translated':
                text = replace_all(text, title_char_map).strip()
            else:
                text = replace_all(text, settings.CHAR_MAP).strip()
            text = format_sections(text)
            metadata_results[field]['fr'] = text
    return metadata_results


def get_existing_figures(layer_directory):
    """ Return list of filenames within directory for figures without extensions. """
    logger.info(f'Getting existing figures from: {layer_directory}')
    filepaths = [os.path.splitext(file)[0] for file in glob.glob(layer_directory + '/*') if file.endswith('.png')]
    return [pathlib.Path(f).as_posix() for f in filepaths]


def get_ds_from_gdb(raster_filepath):
    """Return gdal.Dataset object from a raster_filepath (File Geodatabase and raster layer name)."""
    gdb_path, layer_name = os.path.dirname(raster_filepath), os.path.basename(raster_filepath)
    return gdal.OpenEx(f'OpenFileGDB:"{gdb_path}":{layer_name}') 


def format_caption_units(caption):
    """
    Format caption text with proper LaTeX unit formatting.
    This can be used in the plot_vector, plot_raster, and plot_netcdf functions.
    """
    if not caption:
        return caption
    # Replace known unit patterns
    for pattern, replacement in settings.CHAR_MAP.items():
        caption = caption.replace(pattern, replacement)
    # Handle any remaining patterns like m^-2 or d^-1 that weren't caught
    unit_pattern = r'([a-zA-Z])([⁻²⁻¹]|[\^-][12])'
    def unit_replacer(match):
        base = match.group(1)
        exp = match.group(2)
        if exp.startswith('^'):
            exp = exp[1:]  # Remove the caret
        elif exp.startswith('-'):
            exp = f'-{exp[1:]}'  # Keep the minus sign
        return f'{base}$^{{{exp}}}$'
    caption = re.sub(unit_pattern, unit_replacer, caption)
    return caption


def assign_srs(gdal_dataset):
    """ Return gdal_dataset object with Spatial Reference System assigned. 
         NOT USED... READ ONLY AND SETPROJECTION NOT SUPPORTED FAAAA.
    """
    sr = osr.SpatialReference()
    sr.ImportFromEPSG(settings.SPATIAL_REF)
    gdal_dataset.SetProjection(sr.ExportToWkt())
    return gdal_dataset


def get_band(gdal_dataset):
    """ Return gdal.Band object from gdal.Dataset object. """
    return gdal_dataset.GetRasterBand(1)


def get_array_from_gdal_band(gdal_band):
    """ Return numpy array from gdal.Band object. """
    return gdal_band.ReadAsArray()


def get_transform(gdal_dataset):
    return gdal_dataset.GetGeoTransform()


def get_nodata_gdalraster(gdal_band):
    gdal_no_data = gdal_band.GetNoDataValue()
    if not gdal_no_data:
        gdal_no_data = 0
    return gdal_no_data


def get_extent_gdal(geotransfrom, np_array):
    """ Given a tuple from gdal dataset transform and numpy array, return the extent for matplot plotting. """
    ys, xs = np_array.shape
    ulx, xres, _, uly, _, yres = geotransfrom
    return [ulx, ulx+xres*xs, uly, uly+yres*ys]


def get_masked_array(np_array, nodata):
    """ Return numpy masked array (mask NoData values). """
    return np.ma.masked_values(np_array, nodata)


class metaData(object):
    """ Class to contain general information and metadata pulled from open maps records. """
    def __init__(self, gen_meta, results, filter_dict, language, force_plots):
        self.base_url = gen_meta.get('base_url')
        self.rheader = gen_meta.get('rheader')
        self.lheader = gen_meta.get('lheader')
        self.doc_title = gen_meta.get('doc_title')
        self.mnth_yr = gen_meta.get('mnth_yr')
        self.author = gen_meta.get('author')
        self.bg_header = gen_meta.get('bg_header')
        self.bg_text = gen_meta.get('bg_text')
        self.language = language
        self.figure_number = 2
        self.force_plots = force_plots
        def get_unique_topics(filter_dict):
            topics_list = []
            for k, v in filter_dict.items():
                topic = v.get('topic')
                if topic not in topics_list:
                    topics_list.append(topic) 
            return topics_list
        def get_all_topics(filter_dict):
            return [v.get('topic') for k, v in filter_dict.items()]
        def get_names(results_dict, language, filter_dict):
            logger.info('Creating dictionary of record names by topic...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                data_dict[t] =  [d.get('title_translated').get(language) for d in filtered_dicts]
            return data_dict
        def get_ids(results_dict, filter_dict):
            logger.info('Creating dictionary of record IDs by topic...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                data_dict[t] =  [d.get('id') for d in filtered_dicts]
            return data_dict
        def get_maintainers(results_dict, filter_dict):
            logger.info('Getting dictionary of maintainers...')
            data_dict = {}
            # List of emails to replace (case insensitive)
            emails_to_replace = [
                'charles.hannah@dfo-mpo.gc.ca',
                'amber.holdsworth@dfo-mpo.gc.ca',
                'angelica.pena@dfo-mpo.gc.ca',
                'lu.guan@dfo-mpo.gc.ca',
                'akash.sastri@dfo-mpo.gc.ca'
            ]
            generic_email = 'DFO.PAC.SCI.IOSData-Donnees.ISO.SCI.PAC.MPO@dfo-mpo.gc.ca'
            for t in self.unique_topics:
                # Get ids from filter_dict
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                # Get maintainer emails and replace if needed
                maintainer_emails = []
                for d in filtered_dicts:
                    email = d.get('maintainer_email')
                    if email and any(target.lower() == email.lower() for target in emails_to_replace):
                        email = generic_email
                    maintainer_emails.append(email)
                data_dict[t] = maintainer_emails
            return data_dict
        def get_dates(results_dict, filter_dict):
            logger.info('Getting dictionary of publication dates...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                datestrings = [d.get('metadata_created') for d in filtered_dicts]
                date_times = [datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S.%f') for date_string in datestrings]
                if language == 'fr':
                    # French format: janvier 2023
                    data_dict[t] = [f"{get_french_month(dt.month)} {dt.year}" for dt in date_times]
                else:
                    # English format: January 2023
                    data_dict[t] = [dt.strftime('%B %Y') for dt in date_times]
            return data_dict
        def get_summary_dict(results_dict, language, filter_dict):
            logger.info('Creating dictionary of summaries by topic...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                data_dict[t] = [wrap_http_urls(d.get('notes_translated').get(language)) for d in filtered_dicts]
            return data_dict
        def get_topic_dict(results_dict, filter_dict):
            logger.info('Getting dictionary of topics...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                topic_list =  [d.get('topic_category') for d in filtered_dicts]
                data_dict[t] = [', '.join(topics) for topics in topic_list]
            return data_dict
        def get_keyword_dict(results_dict, language):
            logger.info('Getting dictionary of keywords...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                keyword_list =  [d.get('keywords').get(language) for d in filtered_dicts]
                data_dict[t] = [', '.join(words) for words in keyword_list]
            return data_dict
        def get_urlen_dict(results_dict, filter_dict):
            logger.info('Getting dict of URLs (English) by topic...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                data_dict[t] = [gen_meta.get('base_url') + '/en/dataset/' + d.get('id')  for d in filtered_dicts]
            return data_dict
        def get_urlfr_dict(results_dict, filter_dict):
            logger.info('Getting dict of URLs (French) by topic...')
            data_dict = {}
            for t in self.unique_topics:
                # Get ids from filter_dict.
                ids = [k for k, v in filter_dict.items() if v.get('topic') == t]
                filtered_dicts = (d for id_value in ids for d in results_dict if d.get('id') == id_value)
                data_dict[t] = [gen_meta.get('base_url') + '/fr/dataset/' + d.get('id')  for d in filtered_dicts]
            return data_dict
        def get_matching_resources(results_dict, filter_dict):
            """ Match resources by name instead of ID from the filter dictionary.
            """
            resources = []
            excluded_formats = ['esri rest', 'pdf', 'csv']
            for ds in results_dict:
                filter_id = filter_dict.get(ds.get('id'))
                if not filter_id:
                    logger.info(f"No filter entry for dataset {ds.get('id')}")
                    continue
                ds_resources = ds.get('resources')
                logger.info(f"Found {len(ds_resources)} resources for dataset {ds.get('id')}")
                for res in ds_resources:
                    if (res.get('name') in filter_id.keys() and 
                        res.get('format').lower() not in excluded_formats):
                        resources.append(res)
            if not resources:
                logger.warning('Found no matching resources')
            else:
                logger.info(f'Found {len(resources)} matching resources')
                for res in resources:
                    logger.info(f"Selected: {res.get('name')} ({res.get('format')}) from {res.get('package_id')}")
            return resources
        def download_files(resource, out_dir):
            """Download files with retry logic and progress monitoring."""
            try:
                logger.info('Downloading files...')
                download_url = resource.get('url')
                filename = download_url.split('/')[-1]
                filename = filename.split('\\')[-1]
                filepath = os.path.join(out_dir, filename)
                # If file already exists and is complete, skip download
                if os.path.exists(filepath):
                    logger.info(f'File already exists at {filepath}')
                    return filepath
                # Use requests with stream=True for large files
                max_retries = 3
                retry_count = 0
                while retry_count < max_retries:
                    try:
                        with requests.get(download_url, stream=True) as response:
                            response.raise_for_status()
                            total_size = int(response.headers.get('content-length', 0))
                            logger.info(f'Downloading {filename} ({total_size / (1024*1024):.1f} MB)')
                            with open(filepath, 'wb') as f:
                                chunk_size = 8192
                                downloaded = 0
                                for chunk in response.iter_content(chunk_size=chunk_size):
                                    if chunk:
                                        f.write(chunk)
                                        downloaded += len(chunk)
                                        # Log progress every 5%
                                        if total_size and downloaded % (total_size // 20) < chunk_size:
                                            progress = (downloaded / total_size) * 100
                                            logger.info(f'Download progress: {progress:.1f}%')
                        # Verify file size after download
                        if os.path.getsize(filepath) == total_size:
                            logger.info(f'Download completed successfully: {filepath}')
                            return filepath
                        else:
                            raise Exception("Downloaded file size doesn't match expected size")
                    except Exception as e:
                        retry_count += 1
                        logger.warning(f'Download attempt {retry_count} failed: {str(e)}')
                        if retry_count == max_retries:
                            raise
                        logger.info(f'Retrying download in 5 seconds...')
                        time.sleep(5)
            except Exception as e:
                logger.error('Error downloading resources from Open Maps')
                logger.error(traceback.format_exc())
                raise
        def get_pngs(resources, filter_dict, language):
            data_dict = {}
            logger.info(f"Total resources to process: {len(resources)}")
            logger.info("Resource IDs to process:")
            for res in resources:
                logger.info(f"- {res.get('id')}: {res.get('name_translated').get(language)}")
            for res in resources:
                logger.info('-'*50)
                res_name = res.get('name_translated').get(language)
                logger.info(f'>>>>>>>>>>Processing file {res_name}<<<<<<<<<<')
                package_id = res.get('package_id')
                include_map = True if filter_dict.get(package_id).get('include_map') == 1 else False
                if not include_map:
                    logger.info(f'include_map is {include_map}. Skipping plot.')
                    continue
                topic = filter_dict.get(res.get('package_id')).get('topic')
                res_name = res.get('name')
                # May be a list of resource names of a dictionary whose values are lists of resource names.
                target_lyrs = filter_dict.get(package_id).get(res_name)
                # Check for file geodatabase rasters.
                is_fgdb_raster = True if filter_dict.get(package_id).get('fgdb_raster') == 1 else False
                is_fgdb_both = True if filter_dict.get(package_id).get('fgdb_both') == 1 else False
                flip_raster = True if filter_dict.get(package_id).get('flip_rasters') == 1 else False
                figure_captions = filter_dict.get(package_id).get('captions')
                layer_directory, already_exists = make_dir(package_id)
                # download zip file
                spatial_file = download_files(res, layer_directory)
                file_format = res.get('format').lower()
                unzip_file(spatial_file, layer_directory, file_format)
                if is_fgdb_both:
                    base_lyr = load_base_lyr(settings.BASE_LYR_PATH)
                    # Separate vector and raster layers
                    vector_layers = ['Recreational_Vessels_PointData_BC', 'Recreational_Boating_Model_Results_BC']
                    raster_layers = ['surveyeffort_bc']
                    # Process vectors
                    web_merc_dfs = reproject_gdf(layer_directory, file_format, vector_layers)
                    vector_captions = figure_captions[:len(vector_layers)]
                    raster_captions = figure_captions[len(vector_layers):]
                    # make vector plots
                    plots = [plot_vector(df_dict.get('geodf'), df_dict.get('path'), caption) 
                            for df_dict, caption in zip(web_merc_dfs, vector_captions)]
                    # Process rasters
                    gdb_name = [file for file in os.listdir(layer_directory) if file.endswith('.gdb')][0]
                    raster_filepaths = [os.path.join(layer_directory, gdb_name, t) for t in raster_layers]
                    plots += [plot_raster(raster_file, base_lyr, flip_raster, is_fgdb_raster, caption) 
                            for raster_file, caption in zip(raster_filepaths, raster_captions)]
                elif (file_format in ['shp', 'fgdb/gdb'] and not is_fgdb_raster):
                    # list of dictionaries with keys path and geodf
                    web_merc_dfs = reproject_gdf(layer_directory, file_format, target_lyrs)
                    # make plot
                    plots = [plot_vector(df_dict.get('geodf'), df_dict.get('path'), caption) for df_dict, caption in zip(web_merc_dfs, figure_captions)]
                elif (file_format in ['tiff', 'tif', 'geotif', 'geotiff']) or (file_format == 'fgdb/gdb' and is_fgdb_raster):
                    base_lyr = load_base_lyr(settings.BASE_LYR_PATH)
                    if target_lyrs:
                        if file_format == 'fgdb/gdb':
                            # We need to add the file geodatabase to path.
                            gdb_name = [file for file in os.listdir(layer_directory) if file.endswith('.gdb')][0]
                            raster_filepaths = [os.path.join(layer_directory, gdb_name, t) for t in target_lyrs]
                        else:
                            if isinstance(target_lyrs, dict):
                                raster_filepaths = {}
                                for k, lyr_list in target_lyrs.items():
                                    paths = [os.path.join(layer_directory, lyr) for lyr in lyr_list]
                                    raster_filepaths[k] = paths
                            else:
                                raster_filepaths = [os.path.join(layer_directory, t) for t in target_lyrs]
                    else:
                        if file_format == 'fgdb/gdb':
                            # We need to add the file geodatabase to path.
                            gdb_name = [file for file in os.listdir(layer_directory) if file.endswith('.gdb')][0]
                            lyrs = fiona.listlayers(os.path.join(layer_directory, gdb_name))
                            raster_filepaths = [os.path.join(layer_directory, gdb_name, l) for l in lyrs]
                        else:
                            raster_filepaths = [file for file in glob.glob(layer_directory + '/*') if file.endswith('.tif') or file.endswith('.tiff')]
                    logger.info(raster_filepaths)
                    if isinstance(raster_filepaths, dict):
                        plots = [plot_raster(raster_list, base_lyr, flip_raster, is_fgdb_raster, caption) for raster_list, caption in zip(raster_filepaths.values(), figure_captions)]
                    else:
                        plots = [plot_raster(raster_file, base_lyr, flip_raster, is_fgdb_raster, caption) for raster_file, caption in zip(raster_filepaths, figure_captions)]
                elif file_format == 'netcdf':
                    logger.info('Plotting NetCDF data.')
                    filepaths = [os.path.join(layer_directory, t) for t in target_lyrs]
                    logger.info(filepaths)
                    plots = [plot_netcdf(netcdf_file, caption) for netcdf_file, caption in zip(filepaths, figure_captions)]
                # Have plots for a single resource.
                id_title_dict = {k:v.get('title') for k, v in filter_dict.items() if v.get('topic') == topic}
                logger.info(id_title_dict)
                if topic in data_dict.keys():
                    data_dict[topic][id_title_dict.get(package_id)] = plots
                else:
                    data_dict[topic] = {id_title_dict.get(package_id): plots}
            logger.info(data_dict)
            for topic, datasets in data_dict.items():
                for dataset_name, plots in datasets.items():
                    for plot in plots:
                        png_path = plot[0] + '.png'
                        if not os.path.exists(png_path):
                            logger.error(f'Missing PNG file: {png_path}')
                        else:
                            logger.info(f'Verified PNG exists: {png_path}')
            return data_dict
        def plot_vector(geodataframe_web_merc, vector_path, caption):
            # Format the caption with proper unit handling
            caption = format_caption_units(caption)
            try:
                # caption = f'Figure {self.figure_number}: {caption}'
                png_name_no_ext = os.path.splitext(vector_path)[0]
                png_name_no_ext = pathlib.Path(png_name_no_ext).as_posix()
                png_basename = os.path.basename(png_name_no_ext)
                png_name = png_name_no_ext + '.png'
                # Check if PNG already exists
                if os.path.exists(png_name) and not self.force_plots:
                    logger.info(f'Plot already exists for {png_basename}, skipping plot generation...')
                    short_caption = caption.split('. ')[0] + '.'
                    return png_name_no_ext, caption, short_caption
                logger.info(f'Creating plot for vector layer: {png_basename}...')
                if 'geomorphic_units' in vector_path.lower():
                    colormap = plt.cm.get_cmap('BrBG', len(geodataframe_web_merc['Zone'].unique()))
                    ax = geodataframe_web_merc.plot(column='Zone',
                                                    figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                    cmap=colormap,
                                                    alpha=0.4,
                                                    linewidth=0,
                                                    legend=True)
                    legend = ax.get_legend()
                    if legend:
                        legend.set_title('Zone')
                elif 'recreational_boating_model' in vector_path.lower():
                    # Use a sequential colormap for predicted values
                    colormap = plt.cm.get_cmap('YlOrRd')  # Yellow to Orange to Red colormap
                    ax = geodataframe_web_merc.plot(column='ZINB_PRED',
                                                figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                cmap=colormap,
                                                alpha=0.7,
                                                linewidth=0.1,
                                                legend=True)
                    legend = ax.get_legend()
                    if legend:
                        legend.set_title('Predicted probability of encountering a recreational vessel')
                elif 'bh' in vector_path.lower():
                    # Use a sequential colormap for predicted values
                    colormap = plt.cm.get_cmap('Set3')  # Yellow to Orange to Red colormap
                    ax = geodataframe_web_merc.plot(column='Habitat',
                                                figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                cmap=colormap,
                                                alpha=0.7,
                                                linewidth=0.1,
                                                legend=True,
                                                legend_kwds={'bbox_to_anchor': (1.05, 1),
                                                'loc': 'upper left',
                                                'title': 'Biogenic habitat type'})
                elif 'Cumulative_Impacts_Pacific' in vector_path:
                    # Use a sequential colormap for predicted values
                    colormap = plt.cm.get_cmap('YlOrRd')  # Yellow to Orange to Red colormap
                    ax = geodataframe_web_merc.plot(column='Cumul_Impact_ALL',
                                                figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                cmap=colormap,
                                                alpha=0.7,
                                                linewidth=0.1,
                                                legend=True)
                    legend = ax.get_legend()
                    if legend:
                        legend.set_title('Cumulative impact score representing impacts from all activities and all habitats in each PU grid cell.')
                # New case for whale watching data
                elif any(layer in vector_path for layer in ['Whale_watching_trips_british_columbia', 'Wildlife_viewing_events_BritishColumbia']):
                    colormap = plt.cm.get_cmap('YlOrRd')
                    ax = geodataframe_web_merc.plot(column='vessel_count_2021',
                                                figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                cmap=colormap,
                                                alpha=0.7,
                                                linewidth=0.1,
                                                legend=True)
                    legend = ax.get_legend()
                    if legend:
                        legend.set_title('Vessel Count (2021)')
                else:
                    ax = geodataframe_web_merc.plot(figsize=(settings.FIG_SIZE_VECTOR, settings.FIG_SIZE_VECTOR),
                                                    alpha=0.6,
                                                    linewidth=0.2,
                                                    edgecolor='navy',
                                                    color='deepskyblue')
                # remove x, y ticks (cannot really see them at the scale they are used in the report anyways)
                ax.tick_params(left=False, labelleft=False,
                               bottom=False, labelbottom=False)
                ctx.add_basemap(ax,
                                source=settings.BASEMAP_PROVIDER)
                plt.rcParams.update({'font.size': 14})
                # plt.figtext(0.025, 0.03, caption, wrap=True, horizontalalignment='left', multialignment='left', fontsize=12)
                # Increase the bottom margin to avoid overlap
                plt.tight_layout(rect=[0, .08, 1, 1])
                plt.savefig(png_name, bbox_inches='tight')
                plt.close()
                self.figure_number += 1
                short_caption = caption.split('. ')[0] + '.'
                return png_name_no_ext, caption, short_caption
            except Exception:
                logger.error('Error generating plot')
                logger.error(traceback.format_exc())
        def plot_gdb_raster(raster_filepath, base_lyr, flip_raster, caption):
            # https://stackoverflow.com/questions/68705090/how-a-plot-a-raster-opened-through-gdal-using-matplotlib
            # raster_filepath = r"D:\projects\msp\msp-report-latex\open-maps-data\fed5f00f-7b17-4ac2-95d6-f1a73858dac0\Recreational_Boating_Data_Model.gdb\surveyeffort_bc"
            lyr_name = os.path.basename(raster_filepath)
            png_name_no_ext = os.path.dirname(os.path.dirname(raster_filepath))
            png_name_no_ext = os.path.join(png_name_no_ext, lyr_name)
            png_name_no_ext = pathlib.Path(png_name_no_ext).as_posix()
            png_basename = os.path.basename(png_name_no_ext)
            png_name = png_name_no_ext + '.png'
            # Check if PNG already exists
            if os.path.exists(png_name) and not self.force_plots:
                logger.info(f'Plot already exists for {png_basename}, skipping plot generation...')
                short_caption = caption.split('. ')[0] + '.'
                return png_name_no_ext, caption, short_caption
            logger.info(f'Processing layer: {png_basename} as File Geodatabase Raster.')
            ds = get_ds_from_gdb(raster_filepath)
            if not ds:
                return
            band = get_band(ds)
            data_array = get_array_from_gdal_band(band)
            nd = get_nodata_gdalraster(band)
            masked_array = get_masked_array(data_array, nd)
            origin = 'upper'
            # Special case.
            if lyr_name == 'surveyeffort_bc':
                masked_array = np.ma.masked_array(masked_array, mask=masked_array < 0, fill_value=nd)
                origin = 'lower'
            gt = get_transform(ds)
            extent = get_extent_gdal(gt, masked_array)
            logger.info('Creating plot for {}.'.format(png_basename))
            ax = base_lyr.plot(figsize=(settings.FIG_SIZE_RASTER,
                                        settings.FIG_SIZE_RASTER),
                                        color='lightgrey', 
                                        alpha=0.6)
            # remove x, y ticks (cannot really see them at the scale they are used in the report anyways)
            ax.tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
            # GDAL Band DataTypes --> https://gdal.org/tutorials/raster_api_tut.html
            # http://ikcest-drr.osgeo.cn/tutorial/k8023
            # from osgeo import gdalconst
            # >>> gdalconst.GDT_Int8
            # 14
            data_type = band.DataType
            if data_type in settings.GDAL_INTS.keys():
                cmap = plt.get_cmap('Spectral_r', band.GetMaximum() - band.GetMinimum() + 1)
            else:
                cmap = cm.get_cmap('Spectral_r').copy() # cmap = plt.cm.viridis
            cmap.set_bad('#f5f5f5')
            tif_plot = ax.imshow(masked_array, 
                    cmap=cmap,
                    extent=extent,
                    origin=origin
                )
            if flip_raster:
                # A GIS will transform GeoTIFF and orient correctly, but if we don't flip it, it will be upside down.
                ax.invert_yaxis()
            plt.tight_layout(rect=[0, .08, 1, 1])
            plt.colorbar(tif_plot, ax=ax)
            plt.savefig(png_name, dpi=900)
            plt.close()
            ds, band, masked_array = None, None, None
            self.figure_number += 1
            short_caption = caption.split('. ')[0] + '.'
            return png_name_no_ext, caption, short_caption
        def plot_tif_raster(raster_filepath, base_lyr, flip_raster, caption):
            """ Return list of filepaths to output pngs written to disk. Handle multi-panel figures if RASTER_FILEPATH is a dict. """
            if isinstance(raster_filepath, list):
                png_name_no_ext = os.path.splitext(raster_filepath[0])[0]
                png_name_no_ext = pathlib.Path(png_name_no_ext).as_posix()
                png_basename = os.path.basename(png_name_no_ext)
                logger.info(f'Processing layers: {png_basename}.')
                png_name = png_name_no_ext + '.png'
                # Check if PNG already exists
                if os.path.exists(png_name) and not self.force_plots:
                    logger.info(f'Plot already exists for {png_basename}, skipping plot generation...')
                    short_caption = caption.split('. ')[0] + '.'
                    return png_name_no_ext, caption, short_caption
                fig, axes = plt.subplots(ncols=len(raster_filepath), figsize=(settings.FIG_SIZE_RASTER, settings.FIG_SIZE_RASTER/1.5))
                handles = []  # Collect colorbar handles
                for i, raster_file in enumerate(raster_filepath):
                    raster_src = rio.open(raster_file)
                    raster_band = raster_src.read(1, masked=True)
                    plot_extent = plotting_extent(raster_src)
                    ras_crs = raster_src.crs
                    proj_crs = CRS.from_wkt(ras_crs.wkt)
                    if ras_crs.to_epsg() != base_lyr.crs.to_epsg():
                        base_lyr = base_lyr.to_crs(proj_crs)
                    int_dtypes = ['uint16', 'int16', 'uint32', 'int32']
                    raster_dtype = str(raster_band.dtype)
                    ax = axes[i] if len(raster_filepath) > 1 else axes
                    if i == 0:
                        ax.tick_params(left=True, labelleft=True, bottom=True, labelbottom=True)
                    else:
                        ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)  # Remove tick labels from the figure
                    ax.grid(linestyle = '--', linewidth = 0.5)
                    ax.ticklabel_format(style='plain')
                    base_lyr.plot(ax=ax, color='lightgrey', alpha=0.6)
                    cmap = plt.get_cmap('Spectral_r', np.max(raster_band) - np.min(raster_band) + 1) if raster_dtype in int_dtypes else 'Spectral_r'
                    tif_plot = ax.imshow(raster_band, cmap=cmap, extent=plot_extent)
                    if flip_raster:
                        ax.invert_yaxis()
                    handles.append(tif_plot)  # Add colorbar handle to the list
                    raster_src.close()
                # plt.figtext(0.025, 0.03, caption, wrap=True, horizontalalignment='left', multialignment='left', fontsize=12)
                # Increase the bottom margin to avoid overlap
                plt.tight_layout(rect=[0, .08, 1, 1])
                plt.grid(linestyle = '--', linewidth = 0.5)
                plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)  # Remove tick labels from the figure
                plt.colorbar(handles[0], ax=axes, location='right', shrink=0.75)  # Create a single colorbar
                plt.savefig(png_name, dpi=300)
                plt.close()
            else:
                png_name_no_ext = os.path.splitext(raster_filepath)[0]
                png_name_no_ext = pathlib.Path(png_name_no_ext).as_posix()
                png_basename = os.path.basename(png_name_no_ext)
                logger.info(f'Processing layer: {png_basename}.')
                png_name = png_name_no_ext + '.png'
                # Check if PNG already exists
                if os.path.exists(png_name) and not self.force_plots:
                    logger.info(f'Plot already exists for {png_basename}, skipping plot generation...')
                    short_caption = caption.split('. ')[0] + '.'
                    return png_name_no_ext, caption, short_caption
                raster_src = rio.open(raster_filepath)
                # Set plot extent to that of the raster source layer, not vector basemap.
                plot_extent = plotting_extent(raster_src)
                # Assume single raster band - mask out NoData values.
                raster_band = raster_src.read(1, masked=True)
                ras_crs = raster_src.crs
                proj_crs = CRS.from_wkt(ras_crs.wkt)
                if ras_crs.to_epsg() != base_lyr.crs.to_epsg():
                    # Reproject base layer to match raster layer CRS.
                    base_lyr = base_lyr.to_crs(proj_crs)
                int_dtypes = ['uint16', 'int16', 'uint32', 'int32']
                raster_dtype = str(raster_band.dtype)
                logger.info('Creating plot for {}.'.format(png_basename))
                ax = base_lyr.plot(figsize=(settings.FIG_SIZE_RASTER,
                                            settings.FIG_SIZE_RASTER),
                                            color='lightgrey', 
                                            alpha=0.6)
                ax.tick_params(left=True, labelleft=True,
                                bottom=True, labelbottom=True)
                # Add base layer.
                if raster_dtype in int_dtypes:
                    cmap = plt.get_cmap('Spectral_r', np.max(raster_band) - np.min(raster_band) + 1)
                else:
                    cmap = 'Spectral_r'
                tif_plot = ax.imshow(raster_band, 
                    cmap=cmap,
                    extent=plot_extent
                )
                if flip_raster:
                    # A GIS will transform GeoTIFF and orient correctly, but if we don't flip it, it will be upside down.
                    ax.invert_yaxis()
                # plt.figtext(0.025, 0.03, caption, wrap=True, horizontalalignment='left', multialignment='left', fontsize=12)
                # Increase the bottom margin to avoid overlap
                plt.tight_layout(rect=[0, .08, 1, 1])
                plt.grid(linestyle = '--', linewidth = 0.5)
                plt.ticklabel_format(style='plain')
                if raster_dtype in int_dtypes:
                    plt.colorbar(tif_plot, ax=ax, ticks=np.arange(
                        np.min(raster_band), np.max(raster_band) + 1))
                else:
                    plt.colorbar(tif_plot, ax=ax)
                # A low dpi value results in plots that appear to have gaps in the data which do not exist.
                plt.savefig(png_name, dpi=900)
                # Close figure and plot and raster dataset reader.
                plt.close()
                raster_src.close()
            # Don't increment for each subplot, only once for multi-panel figure.
            self.figure_number += 1
            short_caption = caption.split('. ')[0] + '.'
            # Return full path to PNG file.
            return png_name_no_ext, caption, short_caption
        def plot_raster(raster_filepath, base_lyr, flip_raster, is_fgdb_raster, caption):
            # Format the caption with proper unit handling
            caption = format_caption_units(caption)
            try:
                if is_fgdb_raster:
                    logger.info(f'{raster_filepath} is a File Geodatabase Raster')
                    return plot_gdb_raster(raster_filepath, base_lyr, flip_raster, caption)
                return plot_tif_raster(raster_filepath, base_lyr, flip_raster, caption)
            except Exception:
                logger.error('Error generating plot')
                logger.error(traceback.format_exc())
        def plot_netcdf(file_path, caption):
            # Format the caption with proper unit handling
            caption = format_caption_units(caption)
            png_name_no_ext = os.path.splitext(file_path)[0]
            png_name_no_ext = pathlib.Path(png_name_no_ext).as_posix()
            png_basename = os.path.basename(png_name_no_ext)
            logger.info(f'Processing layer: {png_basename}.')
            png_name = png_name_no_ext + '.png'
            nc_file = nc.Dataset(file_path, 'r')
            # Extract relevant data for the specified timestep
            lats = nc_file.variables['nav_lat'][:]
            lons = nc_file.variables['nav_lon'][:]
            temperature_data = nc_file.variables['temp'][7, 0, :, :]
            label = nc_file.variables['temp'].long_name + f" ({nc_file.variables['temp'].units})"
            nc_file.close()
            projection = ccrs.PlateCarree()
            plt.figure(figsize=(settings.FIG_SIZE_RASTER, settings.FIG_SIZE_RASTER))
            ax = plt.axes(projection=projection)
            im = ax.pcolormesh(lons, lats, temperature_data, transform=projection, cmap='Spectral_r')
            ax.coastlines()
            gl = ax.gridlines(linestyle='--', draw_labels=True)
            gl.xlabels_top = False
            gl.xlabels_bottom = True
            gl.ylabels_left = True
            gl.ylabels_right=False
            plt.colorbar(im, label=label)
            # plt.figtext(0.025, 0.03, caption, wrap=True, horizontalalignment='left', multialignment='left', fontsize=12)
            # Increase the bottom margin to avoid overlap
            plt.tight_layout(rect=[0, .08, 1, 1])
            plt.ticklabel_format(style='plain')
            plt.savefig(png_name, dpi=900)
            plt.close()
            self.figure_number += 1
            short_caption = caption.split('. ')[0] + '.'
            return png_name_no_ext, caption, short_caption
        def reproject_gdf(data_directory, spatial_format, target_lyrs):
            """ spatial_format e.g. -> shp | fgdb/gdb """
            spatial_format = spatial_format.lower()
            try:
                if spatial_format == 'shp':
                    # get paths matching EXTENSION and TARGET_LYRS
                    data_paths = glob.glob(os.path.join(data_directory, f'*.{spatial_format}'))
                    data_lyrs = [os.path.splitext(os.path.basename(d)) for d in data_paths]
                    logger.info(f'Available layers: {data_lyrs}')
                    data_paths = [d for d in data_paths if os.path.splitext(os.path.basename(d))[0] in target_lyrs]
                    logger.info(f'Target layers: {target_lyrs}')
                    # load data as geodataframes
                    open_maps_gdfs = [gp.read_file(data_path) for data_path in data_paths]
                if spatial_format == 'fgdb/gdb':
                    data_paths = glob.glob(os.path.join(data_directory, f'*.gdb'))
                    open_maps_gdfs = []
                    lyrs = fiona.listlayers(data_paths[0])
                    logger.info(f'Available layers: {lyrs}')
                    lyrs = [lyr for lyr in lyrs if lyr in target_lyrs]
                    logger.info(f'Target layers: {target_lyrs}')
                    for lyr in lyrs:
                        open_maps_gdfs.append(gp.read_file(data_paths[0], driver='FileGDB', layer=lyr))
                # convert geometry to EPSG:3857 (Web Mercator) for basemap plot
                # https://spatialreference.org/ref/sr-org/epsg3857-wgs84-web-mercator-auxiliary-sphere/
                logger.info('Reprojecting geodataframes to Web Mercator for plotting...')
                open_maps_gdf_webmerc = [open_maps_gdf.to_crs(epsg=3857) for open_maps_gdf in open_maps_gdfs]
                if spatial_format == 'shp':
                    # return a list of dictionaries with keys path and geodf and value for each from the lists created ^
                    return [{'path': path, 'geodf': gdf} for path, gdf in zip(data_paths, open_maps_gdf_webmerc)]
                if spatial_format == 'fgdb/gdb':
                    return [{'path': os.path.join(data_directory, lyr), 'geodf': gdf} for lyr, gdf in zip(lyrs, open_maps_gdf_webmerc)]
            except Exception:
                logger.error(
                    f'Error reprojecting data to EPSG: 3857 (Web Mercator).')
                logger.error(traceback.format_exc())
        def make_dir(dir_name):
            try:
                # Make output directory if it does not exist already
                out_dir = os.path.join('open-maps-data', dir_name)
                exists = False
                if not os.path.exists(out_dir):
                    # logger.info(f'Creating output directory: {out_dir}')
                    os.makedirs(out_dir)
                else:
                    # logger.info(
                        # f'{out_dir} already exists. Files will be downloaded here.')
                    exists = True
                return out_dir, exists
            except Exception:
                logger.error('Error making directories.')
                logger.error(traceback.format_exc())
        def unzip_file(file_path, out_dir, spatial_format):
            """
            Unzip file but ignore internal directory structure (flatten to one directory).
            Supports both .zip and .7z formats.
            Args:
                file_path (str): Path to the compressed file
                out_dir (str): Directory to extract files to
                spatial_format (str): Format specification, special handling for 'fgdb/gdb'
            Returns:
                str: Path to the processed file
            """
            ext = os.path.splitext(file_path)[-1].lower()
            if ext not in ['.zip', '.7z']:
                logger.info('No compressed file to extract...')
                return file_path
            try:
                logger.info(f'Extracting {ext} file...')
                if ext == '.zip':
                    return _handle_zip(file_path, out_dir, spatial_format)
                else:  # .7z
                    return _handle_7z(file_path, out_dir, spatial_format)
            except Exception as e:
                logger.error(f'Error extracting resources: {str(e)}')
                logger.error(traceback.format_exc())
                return file_path
        def _handle_zip(file_path, out_dir, spatial_format):
            """Handle extraction of ZIP files"""
            if spatial_format.lower() == 'fgdb/gdb':
                with zipfile.ZipFile(file_path, 'r') as zip_file:
                    zip_file.extractall(out_dir)
                return file_path
            with zipfile.ZipFile(file_path, 'r') as zip_file:
                for member in zip_file.namelist():
                    filename = os.path.basename(member)
                    # skip directories
                    if not filename:
                        continue
                    # copy file (taken from zipfile extract)
                    source = zip_file.open(member)
                    target = open(os.path.join(out_dir, filename), 'wb')
                    with source, target:
                        shutil.copyfileobj(source, target)
            return file_path
        def _handle_7z(file_path, out_dir, spatial_format):
            """Handle extraction of 7z files"""
            if spatial_format.lower() == 'fgdb/gdb':
                with py7zr.SevenZipFile(file_path, 'r') as sz_file:
                    sz_file.extractall(out_dir)
                return file_path
            # For flattened extraction
            with py7zr.SevenZipFile(file_path, 'r') as sz_file:
                # Get list of all files
                file_list = sz_file.getnames()
                # Create a temporary directory for initial extraction
                temp_dir = os.path.join(out_dir, 'temp_extract')
                os.makedirs(temp_dir, exist_ok=True)
                # Extract all files to temporary directory
                sz_file.extractall(temp_dir)
                # Move files to final directory, flattening the structure
                for root, _, files in os.walk(temp_dir):
                    for file in files:
                        source_path = os.path.join(root, file)
                        target_path = os.path.join(out_dir, file)
                        shutil.move(source_path, target_path)
                # Clean up temporary directory
                shutil.rmtree(temp_dir)
            return file_path
        self.unique_topics = get_unique_topics(filter_dict)
        self.topics = get_all_topics(filter_dict)
        self.name_dict = get_names(results, self.language, filter_dict)
        self.id_dict = get_ids(results, filter_dict)
        self.maintainer_dict = get_maintainers(results, filter_dict)
        self.date_dict = get_dates(results, filter_dict)
        self.summary_dict = get_summary_dict(results, self.language, filter_dict)
        self.topic_dict = get_topic_dict(results, filter_dict)
        self.keyword_dict = get_keyword_dict(results, self.language)
        self.urlen_dict = get_urlen_dict(results, filter_dict)
        self.urlfr_dict = get_urlfr_dict(results, filter_dict)
        self.matching_resources = get_matching_resources(results, filter_dict)
        self.png_dict = get_pngs(self.matching_resources, filter_dict, self.language)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'id_file', type=str, help='csv file with columns [id, title, ex_res, ex_lyrs]. \
        id is Open Maps ID, ex_res is an example resource to download. ex_lyrs are layer names within the resource to map.')
    parser.add_argument('language', type=str, choices=['en', 'fr'], help='English (en) or French (fr).')
    parser.add_argument('--force-plots', action='store_true', help='Force regeneration of plots even if they already exist')
    args = parser.parse_args()

    # Get results for open maps records, filter out None results (if no ID exists or no spatial resources).
    open_maps_filter = create_id_dict(args.id_file)
    open_maps_results = [get_om_results(open_id) for open_id in open_maps_filter.keys()]
    open_maps_results = list(filter(lambda item: item is not None, open_maps_results))

    # Create object from metadata class. In hindsight, this was not the best approach.
    # Would be better for one object per metadata record, but oh well.
    meta_object = metaData(settings.DOCUMENT_METADATA,
                           open_maps_results,
                           open_maps_filter,
                           args.language,
                           force_plots=args.force_plots)
    logger.info("Dataset names:")
    for topic, names in meta_object.name_dict.items():
        logger.info(f"Topic: {topic}")
        for name in names:
            logger.info(f"  Name: {name}")
    # Write new latex document, i.e., fill template with metadata.
    # The encoding bit here is critical, without it, the output .tex file is not utf-8 encoded.
    # in a terminal, type `file -i <filename>` such as:
    # file -i msp-report.tex What we want to see is --> msp-report.tex: text/x-tex; charset=utf-8
    logger.info('Loading template...')
    if args.language == 'en':
        template = settings.LATEX_JINJA_ENV.get_template(settings.TEX_METADATA_TEMPLATE_EN)
    else:
        template = settings.LATEX_JINJA_ENV.get_template(settings.TEX_METADATA_TEMPLATE_FR)
    base_filename = settings.BASE_NAME + args.language
    tex_document = base_filename + '.tex'
    logger.info(f'Populating template with metadata and producing output file: {tex_document}')
    with open(tex_document, 'w', encoding=settings.ENCODING) as tex_file:
        tex_file.write(template.render(rheader=meta_object.rheader,
                                        lheader=meta_object.lheader,
                                        title=meta_object.doc_title,
                                        author=meta_object.author,
                                        date=meta_object.mnth_yr,
                                        background=meta_object.bg_header,
                                        background_text=meta_object.bg_text,
                                        unique_topics=meta_object.unique_topics,
                                        all_topics=meta_object.topics,
                                        dataset_names=meta_object.name_dict,
                                        open_ids=meta_object.id_dict,
                                        url_dict_en=meta_object.urlen_dict,
                                        url_dict_fr=meta_object.urlfr_dict,
                                        notes=meta_object.summary_dict,
                                        topic_category=meta_object.topic_dict,
                                        keywords=meta_object.keyword_dict,
                                        maintainer=meta_object.maintainer_dict,
                                        release_date=meta_object.date_dict,
                                        map_plot=meta_object.png_dict))

    # Convert latex output to PDF file.
    # Need to compile multiple times for TOC operation and bibliography: https://tex.stackexchange.com/a/301109
    # Need to call biber to create bibliography: https://tex.stackexchange.com/a/34136
    logger.info('Converting LaTeX output to PDF file.')
    subprocess.call(['pdflatex', tex_document])
    subprocess.call(['bibtex', base_filename])
    subprocess.call(['pdflatex', tex_document])
    subprocess.call(['pdflatex', tex_document])


if __name__ == '__main__':
    main()
