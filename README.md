# Marine Spatial Planning (MSP) Data Report

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060

- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)

---

## Objective

The purpose of this project is to generate a bilingual (English/French) technical report in PDF format documenting spatial datasets published to the Open Maps data management platform, submitted by Pacific Science, under Marine Spatial Planning (MSP). The report includes metadata and visualization of datasets that support MSP in five planning areas across Canada, with focus on the Northern Shelf bioregion and the Strait of Georgia and Southern Shelf bioregions.

---

## Summary

Under MSP, datasets are created and submitted to Open Maps for dissemination. This tool documents published layers and provides a comprehensive overview document. The input is a CSV file with Open Maps IDs and additional configuration parameters used to query the CKAN API and retrieve metadata. Each record is processed and added to the output document. The output is a PDF file with maps of the spatial data and associated metadata, organized by ecological features, oceanographic datasets, and human activities.

---

## Status

In-development

---

## Contents

### Project Structure
```
├── msp-env.yml                 # Conda environment specification
├── msp_report.py              # Main Python script
├── settings.py                # Configuration settings
├── CJFAS.bst                  # Bibliography style from TechnicalReport
├── references_en.bib          # English bibliography
├── references_fr.bib          # French bibliography
├── ids.csv                    # English dataset configurations
├── ids_fr.csv                 # French dataset configurations
├── reference/                 # Reference data
│   ├── canada.png
│   ├── dfo.png
│   ├── north_america.*        # Base map shapefiles
│   └── overview-map.jpg
└── tex_files/                 # LaTeX templates
    ├── abstract.tex
    ├── acknowledgements_*.tex
    ├── conclusion_*.tex
    ├── copyright.tex
    ├── doc_description.tex
    ├── introduction_*.tex
    ├── msp_report_template_*.tex
    ├── tech_description.tex
    └── title.tex
```
Note: `*_en.tex` and `*_fr.tex` indicate English and French versions

### Input Files

##### Dataset Configuration CSV
Required columns:
- id: Record ID from Open Maps
- title: Dataset title
- ex_res: Resource names for example visualization
- ex_lyrs: Layer names to map (comma-separated)
- ex_captions: Figure captions (!!!-separated)
- fgdb_raster: Boolean (1/0) for File Geodatabase raster
- fgdb_both: Boolean (1/0) for mixed vector/raster data
- flip_rasters: Boolean (1/0) for raster orientation
- topic: Topic category for grouping
- include_map: Boolean (1/0) for map generation

---

## Methods

### LaTeX Templates

The report uses a modified version of the [CSAS Technical Report Template](https://github.com/grinnellm/TechnicalReport) to generate the PDF output. Templates define:
- Document structure and formatting
- Bilingual support (English/French)
- Bibliography using CJFAS style
- Section organization by ecological feature

### Python Script (msp_report.py)

1. Process input CSV configuration
2. Initialize logging
3. Create metadata dictionary
4. Set up Jinja2 environment
5. MetaData class functions:
   - Retrieve/clean Open Maps metadata
   - Download/process spatial data
   - Generate visualizations
   - Organize by topic
6. Create output using language-specific template
7. Convert to PDF (pdflatex/bibtex)

---

## Requirements

### LaTeX Installation
* [MiKTeX](https://miktex.org/download)

### Python Environment
```bash
git clone git@gitlab.com:dfo-msea/msp-report-latex.git
cd msp-report-latex
conda env create -f msp-env.yml
conda activate msp-venv
```

### Required Python Packages
- gdal (v3.7.3)
- geopandas
- matplotlib
- netCDF4
- cartopy
- contextily
- fiona
- rasterio
- numpy
- jinja2

### Usage
```bash
python msp_report.py input_file.csv language
```
Where language is 'en' or 'fr'

---

## Caveats

* Requires LaTeX installation
* Requires Anaconda/Miniconda
* Limited input validation
* Assumes spatial resources exist
* File Geodatabase limitations:
  - Processes first .gdb only
  - Single vector layer
  - Special handling for mixed data

---

## Uncertainty

* Raster orientation may need adjustment
* Limited NetCDF visualization
* Base map visibility varies with extent
* Memory intensive for large datasets

---

## Acknowledgements

- Marine Spatial Ecology and Analysis group at DFO
- [CSAS Technical Report Template](https://github.com/grinnellm/TechnicalReport) for LaTeX template and bibliography style

---

## References

* [CKAN API](https://docs.ckan.org/en/2.9/api/)
* [GeoPandas](https://geopandas.readthedocs.io/)
* [Contextily](https://contextily.readthedocs.io/)
* [Jinja2](https://jinja.palletsprojects.com/)
* [CSAS Technical Report](https://github.com/grinnellm/TechnicalReport)