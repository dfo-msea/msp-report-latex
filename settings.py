""" This file containts global variables and dictionaries used
     throughout the main program.
"""
import os
from datetime import date
import jinja2
import pathlib
import contextily as ctx
import re
# For basemap provider, choose one of the following keys.
# ctx.providers.keys()
# dict_keys(['OpenStreetMap', 'OpenSeaMap', 'OpenPtMap', 'OpenTopoMap', 'OpenRailwayMap', 'OpenFireMap', 
# 'SafeCast', 'Thunderforest', 'OpenMapSurfer', 'Hydda', 'MapBox', 'Stamen', 'Esri', 'OpenWeatherMap', 'HERE', 'FreeMapSK', 
# 'MtbMap', 'CartoDB', 'HikeBike', 'BasemapAT', 'nlmaps', 'NASAGIBS', 'NLS', 'JusticeMap', 'Wikimedia', 'GeoportailFrance', 'OneMapSG'])

# Globals
PACKAGE_SHOW = 'https://open.canada.ca/data/en/api/3/action/package_show?id='
PWD = os.getcwd()
LOG_DIR = os.path.join(PWD, 'logs')
BASE_LYR_PATH = os.path.join(PWD, 'reference', 'north_america.shp')
FIG_SIZE_VECTOR = 9
FIG_SIZE_RASTER = 9
SPATIAL_TYPES = ['shp', 'tiff', 'tif', 'geotif', 'fgdb/gdb']
# https://gdal.org/doxygen/gdal_8h.html
GDAL_INTS = {2:'uint16', 3: 'int16', 4: 'uint32', 5: 'int32', 14:'int8'}
SPATIAL_REF = 4326
BASEMAP_PROVIDER = ctx.providers.CartoDB.Positron
BASE_NAME = 'msp-report-'
TEX_DIRECTORY = os.path.join(PWD, 'tex_files')
TEX_DOCUMENT = BASE_NAME + '.tex'
TEX_METADATA_TEMPLATE_EN = 'msp_report_template_en.tex'
TEX_METADATA_TEMPLATE_FR = 'msp_report_template_fr.tex'
ENCODING = 'utf-8-sig'


# Characters to map to (LaTeX formatting).
# https://www.stevesque.com/symbols/
CHAR_MAP = {
    '%': '\\%',
    '_': '\\_',
    "&": "\\&",
    "^": r"\^{}",
    "»": r"\guillemotright",
    "«": r"\guillemotleft",
    '−': '-',
    # Unit superscripts (both Unicode and ASCII)
    '⁻': '$^{-}$',  # Unicode superscript minus
    '²': '$^{2}$',  # Unicode superscript 2
    '³': '$^{3}$',  # Unicode superscript 3
    '⁻²': '$^{-2}$',  # Combined Unicode superscript -2
    '⁻¹': '$^{-1}$',  # Combined Unicode superscript -1
    'm⁻²': 'm$^{-2}$',  # Complete unit with Unicode superscript
    'd⁻¹': 'd$^{-1}$',  # Complete unit with Unicode superscript
    'm^-2': 'm$^{-2}$',  # ASCII version
    'd^-1': 'd$^{-1}$',  # ASCII version
    # Common unit combinations
    'mmol m⁻² d⁻¹': 'mmol m$^{-2}$ d$^{-1}$',  # Full unit with Unicode
    'mmol m^-2 d^-1': 'mmol m$^{-2}$ d$^{-1}$',  # Full unit with ASCII
}
# CHAR_MAP = {"_": " ",
#             "’": "'",  # https://www.andy-roberts.net/writing/latex/formatting
#             "‘": "'",
#             "`": "'",
#             "”": "'",
#             "“": "'",
#             '"': "'",
#             "\\": "/",
#             "\r\n": u"\u000A",
#             "&": "\&",
#             "–": "--",
#             "—": "--",
#             "−": "-",
#             "# ": "",
#             "##": "",
#             "###": "",
#             "####": "",
#             "#####": "",
#             "%": r"\%",
#             "^": r"\^{}",
#             "#": r"\#",
#             "≥": "$\geq$",
#             ">": "$>$",
#             "≤": "$\geq$",
#             "<": "$<$",
#             "±": "$\pm$",
#             "×": r"$\times$",
#             "÷": r"$\div$",
#             "∼": r"$\sim$",
#             "˜": r"$\sim$",
#             "°": "$^{\circ}$",
#             "◦": "$^{\circ}$",
#             "ÿ": "",
#             "•": "",
#             "µ": r"$\mu$",
#             "π": "$\pi$",
#             "χ": "$\chi$"}

DOCUMENT_METADATA = {'base_url': 'https://open.canada.ca/data',
                          'rheader': 'Fisheries and Oceans Canada',
                          'lheader': 'Pacific Science',
                          'doc_title': 'Spatial Data to Support Marine Spatial Planning in the Pacific Region',
                          'mnth_yr': date.today().strftime('%B %Y'),
                          'author': 'Pacific Science',
                          'bg_header': 'Background',
                          'bg_text': 'Lorem ipsum bla bla bla.'}

    # Jinja2 env
LATEX_JINJA_ENV = jinja2.Environment(
        block_start_string='\BLOCK{',
        block_end_string='}',
        variable_start_string='\VAR{',
        variable_end_string='}',
        comment_start_string='\#{',
        comment_end_string='}',
        line_statement_prefix='%%',
        line_comment_prefix='%#',
        trim_blocks=True,
        autoescape=False,
        loader=jinja2.FileSystemLoader(pathlib.Path(TEX_DIRECTORY).as_posix())
    )
